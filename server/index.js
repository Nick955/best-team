var express = require('express');
var app = express();


app.use('/public', express.static(__dirname + './../public'));

// check token
var HRAuth = require('./app/middlewares/auth.js');
app.use(HRAuth.checkAuth);

// parse incoming body
var bodyParser = require('body-parser');
app.use(bodyParser.json());

// routes
var routes = require('./routes');
routes.routes(app);

app.listen(1488, function () {
    console.log('STARTOVAT\' SERVAKI NE BROSIM PORT: 1488');
});
