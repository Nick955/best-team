(function () {
    'use strict';

    angular
        .module('hr')
        .config(['$stateProvider', '$urlRouterProvider', routes]);

    function routes ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: '/public/templates/login/login.tpl.html',
                controller: 'LoginCtrl',
                controllerAs: 'login'
            })
            .state('me', {
                url: '/me',
                templateUrl: '/public/templates/login/main-page.tpl.html',
                controller: 'MainCtrl',
                controllerAs: 'me'
            });

        $urlRouterProvider.otherwise('/login');
    }

}());
